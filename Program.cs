﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace Utility
{
    class RevenueLoadPinger
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();

            //DAILY REVENUE LOAD
            try
            {
                using (CookieAwareWebClient c = new CookieAwareWebClient())
                {
                    int timeout;
                    if (int.TryParse(ConfigurationManager.AppSettings["TimeoutInMilliseconds"], out timeout))
                        c.TimeOut = timeout;

                    Uri uri = new Uri(ConfigurationManager.AppSettings["RevenueUrlToPing"]);
                    Console.WriteLine(string.Format("Pinging {0}", uri.AbsoluteUri));
                    sw.Start();
                    string result = c.DownloadString(uri);
                    sw.Stop();
                    Console.WriteLine(result);
                    Console.WriteLine(string.Format("Completed in {0} seconds", sw.Elapsed.TotalSeconds));
                }
            }
            catch (Exception ex)
            {
                File.WriteAllText("RevenueResultErr.txt", "Stopwatch: " + sw.Elapsed.TotalSeconds + Environment.NewLine + ex.ToString());
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
